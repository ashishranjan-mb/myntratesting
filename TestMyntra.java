import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestMyntra {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "/home/ashish/Desktop/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.myntra.com");

        testMouseHover(driver);
        testScroll(driver);
        testSearchProduct(driver);
        testLogin(driver);
        testSelectingProductAndAddToCart(driver);

    }
        public static void testMyMyntra() throws InterruptedException {
            WebElement ele = driver.findElement(By.className("desktop-userTitle"));
            ele.click();
            Thread.sleep(3000);
            WebElement login = driver.findElement(By.className("desktop-linkButton"));
            login.click();
            Thread.sleep(2000);
            WebElement mobileNumber = driver.findElement(By.className("mobileNumberInput"));
            mobileNumber.sendKeys("8083787014");
            mobileNumber.click();

            WebElement conti_nue = driver.findElement(By.className("submitBottomOption"));
            conti_nue.click();
            Thread.sleep(31000);
            conti_nue.click();

            WebElement passwrd = driver.findElement(By.id("bottomeLink"));
            passwrd.click();
        }
         public static void testMouseHover(WebDriver driver) throws InterruptedException {
            Actions aa = new Actions(driver);
            aa.moveToElement(driver.findElement(By.className("desktop-userTitle"))).build().perform();
            Thread.sleep(3000);
        }
         public static void testScroll(WebDriver driver) throws InterruptedException {
            Actions aa = new Actions(driver);
            aa.sendKeys(Keys.PAGE_DOWN).build().perform();
            Thread.sleep(3000);

            aa.sendKeys(Keys.PAGE_UP).build().perform();
        }
        public static void testSearchProduct(WebDriver driver) throws InterruptedException {
            driver.findElement(By.xpath("//header/div[2]/div[3]/input[1]")).sendKeys("T Shirts");
            Thread.sleep(2000);
            driver.findElement(By.xpath("//header/div[2]/div[3]/a[1]/span[1]")).click();

        }
        public static void testSelectingProductAndAddToCart(WebDriver driver){
        driver.findElement(By.className("product-imageSliderContainer")).click();
        ArrayList<String> AllTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(AllTabs.get(1));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[2]/div/div/div/main/div[2]/div[2]/div[2]/div[2]/div[2]/div[1]/button/p")).click();
        Thread.sleep(3000);

        driver.findElement(By.xpath("/html/body/div[2]/div/div/div/main/div[2]/div[2]/div[3]/div/div[1]")).click();
        Thread.sleep(3000);

        driver.findElement(By.xpath("/html/body/div[2]/div/div/div/main/div[2]/div[2]/div[3]/div/a")).click();

        }

    }

